import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({

});

class LoginForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const field = event.target.name;
    this.setState({ [field]: event.target.value });
  }


  handleSubmit(event) {
    const {
      email,
      password
    } = this.state;

    event.preventDefault();
    if (email && password) {
      this.props.login(email, password);
    }
  }


  render() {

    return (
      <form onSubmit={this.handleSubmit}>
        Email: <input type='text' name='email' value={this.state.email} onChange={this.handleChange}></input>
        Password: <input type='password' name='password' value={this.state.password} onChange={this.handleChange}></input>
        <input type="submit" value="Submit"></input>
      </form>
    );
  }
}

LoginForm.propTypes = {
  login: PropTypes.func
};

export default withStyles(styles)(LoginForm);

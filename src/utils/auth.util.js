const AUTH = 'AUTH';

export const setStoredAuthState = (idToken, expires_at) => {
  const localStorageState = {
    expires_at,
    idToken,
  };

  localStorage.setItem(AUTH, JSON.stringify(localStorageState));
};

export const removeStoredAuthState = () => {
  localStorage.removeItem(AUTH);
};

export const getStoredAuthState = () => {
  try {
    return JSON.parse(localStorage.getItem(AUTH) || '{}');
  } catch (err) {
    removeStoredAuthState();
    return {};
  }
};

export const isAuthenticated = () => {
  // Check whether the current time is past the
  // access token's expiry time
  let expiresAt = getStoredAuthState()['expires_at'];
  return new Date().getTime() < expiresAt;
};

export const authHeader = () => {
  // return authorization header with jwt token
  let auth = getStoredAuthState();

  if (auth && auth.idToken) {
    return { 'Authorization': 'Bearer ' + auth.idToken };
  } else {
    return {};
  }
};
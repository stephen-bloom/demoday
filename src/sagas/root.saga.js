import { all, takeEvery } from 'redux-saga/effects';
import {
  authConstants
} from '../constants';
import { loginRequestSaga, loginSuccessSaga, loginFailureSaga } from './user/loginRequest.saga';

export function* rootSaga() {
  yield all([
    ////
    // Auth Sagas
    ////
    takeEvery(authConstants.LOGIN_REQUEST, loginRequestSaga),
    takeEvery(authConstants.LOGIN_SUCCESS, loginSuccessSaga),
    takeEvery(authConstants.LOGIN_FAILURE, loginFailureSaga),
  ]);
}
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({

});

class SignupSchedule extends PureComponent {

  render() {
    const {
      finishSignup
    } = this.props;

    return (
      <div>
        <h1>This is where the user sets up their schedule</h1>
        <span onClick={finishSignup}>"Signup Complete" go to Dashboard</span>
      </div>
    );
  }
}

SignupSchedule.propTypes = {
  finishSignup: PropTypes.func
};

export default withStyles(styles)(SignupSchedule);

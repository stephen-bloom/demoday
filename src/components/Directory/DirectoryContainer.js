import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Directory from './Directory';


const mapStateToProps = state => {
  return {

  }
};

const mapDispatchToProps = dispatch => bindActionCreators({

}, dispatch);

const DirectoryContainer = connect(mapStateToProps, mapDispatchToProps)(Directory);

export default DirectoryContainer;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({

});

class Home extends PureComponent {

  render() {
    const {
      loginNav,
      signupNav
    } = this.props;

    return (
      <div>
        <h1>This is where the app will land by default</h1>
        <span>Home component</span>
        <span>Intro and explanation materials of this app</span>
        <br></br>
        <br></br>
        <span onClick={loginNav}>Click here to log in</span>
        <br></br>
        <span onClick={signupNav}>Click here to register</span>
      </div>
    );
  }
}

Home.propTypes = {
  loginNav: PropTypes.func,
  signupNav: PropTypes.func
};

export default withStyles(styles)(Home);

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({

});

class Schedule extends PureComponent {

  render() {
    return (
      <div>
        <h1>This is where BA users will set their schedules</h1>
      </div>
    );
  }
}

Schedule.propTypes = {};

export default withStyles(styles)(Schedule);

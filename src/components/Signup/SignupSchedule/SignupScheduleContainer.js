import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';

import SignupSchedule from './SignupSchedule';


const mapStateToProps = state => {
  return {

  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  finishSignup: () => push('/dashboard')
}, dispatch);

const SignupScheduleContainer = connect(mapStateToProps, mapDispatchToProps)(SignupSchedule);

export default SignupScheduleContainer;

import { push } from 'connected-react-router';
import { call, put } from 'redux-saga/effects';
import { Auth0 } from '../../integrations/index';
import { removeStoredAuthState, setStoredAuthState } from '../../utils/index';
import { loginFailure, loginSuccess, fetchProfileRequest } from '../../actions/index';

export function* loginRequestSaga(action) {
  // Get the auth0 object from the auth utils
  const authHelper = new Auth0();

  try {
    const authResult = yield call(authHelper.login, action.email, action.password);

    const expires_at = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    const token = authResult.accessToken;

    // SUDO: Set the token in local storage/store
    yield put(loginSuccess(token, expires_at));

    // SUDO: Go to intended URL (Home or Redirect)
    yield put(push('/dashboard'));
    // yield put(fetchProfileRequest());
  } catch (error) {
    console.log(error);
    yield put(loginFailure(error));
    yield put(push('/login'));
  }
}

export function* loginSuccessSaga(action) {
  const { token, expires_at } = action;
  yield setStoredAuthState(token, expires_at);
}

export function* loginFailureSaga() {
  yield call(removeStoredAuthState);
}


export function* logoutSaga() {
  removeStoredAuthState();
  yield put(push('/'));
}



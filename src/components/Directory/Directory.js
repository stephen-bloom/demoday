import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({

});

class Directory extends PureComponent {

  render() {
    return (
      <div>
        <h1>This is where the users will see a directory of other users</h1>
      </div>
    );
  }
}

Directory.propTypes = {};

export default withStyles(styles)(Directory);

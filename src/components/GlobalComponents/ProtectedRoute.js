import React, { PureComponent } from 'react';
import { bindActionCreators } from "redux";

import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux';
import { isAuthenticated } from '../../utils';
// import { fetchProfileRequest } from '../../../actions/user.actions';

////
// TODO: Build in redirect to intended route after sign in
////

////
// CODEFROM: https://stackoverflow.com/a/48497783/3609124
////

class ProtectedRouteComp extends PureComponent {

  componentDidMount() {
    const {
      is_authenticated,
      // fetchProfileRequest,
      fetching,
      fetch_needed
    } = this.props;

    if (is_authenticated && !fetching && fetch_needed) {
      // fetchProfileRequest();
    }
  }

  render() {
    const {
      component: Component,
      is_authenticated,
      ...props
    } = this.props;

    return (
      <Route
        {...props}
        render={props => (
          is_authenticated ?
            <Component {...props} /> :
            <Redirect to='/login' />
        )}
      />
    )
  }

}

ProtectedRouteComp.propTypes = {
  fetching: PropTypes.bool,
  fetch_needed: PropTypes.bool,
  is_authenticated: PropTypes.bool
};

const mapStateToProps = (state) => {
  const fetching = state.user.fetching;
  const fetch_needed = state.user.fetch_needed;

  return {
    fetching,
    fetch_needed,
    is_authenticated: isAuthenticated(),
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  // fetchProfileRequest: () => fetchProfileRequest()
}, dispatch);

const ProtectedRoute = connect(mapStateToProps, mapDispatchToProps)(ProtectedRouteComp);

export default ProtectedRoute;
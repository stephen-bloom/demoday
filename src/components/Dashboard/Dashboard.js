import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({
  col: {
    display: 'flex',
    flexDirection: 'column'
  }
});

class Dashboard extends PureComponent {

  render() {
    const {
      classes,
      settingsNav,
      scheduleNav,
      historyNav,
      directoryNav,
      profileNav
    } = this.props;

    return (
      <div className={classes.col}>
        <h1>This is where the app will land after user logs in</h1>
        <span onClick={settingsNav}>Settings</span>
        <span onClick={scheduleNav}>Schedule</span>
        <span onClick={historyNav}>History</span>
        <span onClick={directoryNav}>Directory</span>
        <span onClick={profileNav}>Profile</span>
      </div>
    );
  }
}

Dashboard.propTypes = {
  settingsNav: PropTypes.func,
  scheduleNav: PropTypes.func,
  directoryNav: PropTypes.func,
  profileNav: PropTypes.func
};

export default withStyles(styles)(Dashboard);

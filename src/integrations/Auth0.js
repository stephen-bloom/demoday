import { history } from '../store/store';
import auth0 from 'auth0-js';
import { AUTH_CONFIG } from './Auth0.config';

export class Auth0 {
  auth0 = new auth0.Authentication({
    domain: AUTH_CONFIG.domain,
    clientID: AUTH_CONFIG.clientId,
    redirectUri: AUTH_CONFIG.callbackUrl,
    // audience: AUTH_CONFIG.audience,
    responseType: 'token id_token',
    // scope: 'openid'
  });

  webAuth = new auth0.WebAuth({
    domain: AUTH_CONFIG.domain,
    clientID: AUTH_CONFIG.clientId,
    redirectUri: AUTH_CONFIG.callbackUrl,
  });


  constructor() {
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.changePassword = this.changePassword.bind(this);
  }

  login(email, password) {
    return new Promise((resolve, reject) => {
      this.auth0.loginWithDefaultDirectory({ username: email, password: password },
        ((err, results) => {
          if (err) { reject(err) }
          if (results) { resolve(results) }
        })
      )
    })
  }

  handleAuthentication() {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        history.replace('/');
      } else if (err) {
        history.replace('/login');
        console.log(err);
        alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  setSession(authResult) {
    // Set the time that the access token will expire at
    let expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    // navigate to the home route
    history.replace('/');
  }

  logout() {
    // Clear access token and ID token from local storage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    // navigate to the home route
    history.replace('/login');
  }

  changePassword(email) {
    const options = {
      email,
      connection: 'Username-Password-Authentication'
    }

    return new Promise((resolve, reject) => {
      this.webAuth.changePassword(options,
        (err, results) => {
          if (err) { reject(err) }
          if (results) { resolve(results) }
        }
      )
    })
  }
}
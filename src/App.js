import React, { Component } from "react";
import { Provider } from "react-redux";
import { ConnectedRouter } from 'connected-react-router'
import { Redirect, Route, Switch } from 'react-router-dom'
import store, { history } from "./store/store";

import ProtectedRoute from './components/GlobalComponents/ProtectedRoute';
import LoginContainer from './components/Login/LoginContainer';
import HomeContainer from './components/Home/HomeContainer';
import DashboardContainer from './components/Dashboard/DashboardContainer';
import SettingsContainer from './components/Settings/SettingsContainer';
import ScheduleContainer from './components/Schedule/ScheduleContainer';
import HistoryContainer from './components/History/HistoryContainer';
import DirectoryContainer from './components/Directory/DirectoryContainer';
import SignupContainer from './components/Signup/SignupContainer';
import DemoContainer from './components/Demo/DemoContainer';
import ProfileContainer from './components/Profile/ProfileContainer';


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Switch>
            <Route path='/home' component={HomeContainer} />
            <Route path='/login' component={LoginContainer} />
            <Route path='/signup' component={SignupContainer} />
            <Redirect exact={true} from='/' to="/home" />
            <ProtectedRoute path='/dashboard' component={DashboardContainer} />
            <ProtectedRoute path='/settings' component={SettingsContainer} />
            <ProtectedRoute path='/schedule' component={ScheduleContainer} />
            <ProtectedRoute path='/history' component={HistoryContainer} />
            <ProtectedRoute path='/directory' component={DirectoryContainer} />
            <ProtectedRoute path='/demo/:demoId' component={DemoContainer} />
            <ProtectedRoute path='/user/:userId' component={ProfileContainer} />
          </Switch>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;

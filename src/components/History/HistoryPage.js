import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({
  col: {
    display: 'flex',
    flexDirection: 'column'
  }
});

class HistoryPage extends PureComponent {

  render() {
    const {
      classes,
      demoNav
    } = this.props;

    return (
      <div className={classes.col}>
        <h1>This is where users will see their history of demos</h1>
        <h2>Below is an example list of links to demo details</h2>
        <span onClick={demoNav.bind(this, '123')}>123</span>
        <span onClick={demoNav.bind(this, 'abc')}>abc</span>
      </div>
    );
  }
}

HistoryPage.propTypes = {
  demoNav: PropTypes.func
};

export default withStyles(styles)(HistoryPage);

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';

import SignupInfo from './SignupInfo';


const mapStateToProps = state => {
  return {

  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  signupDemoNav: () => push('/signup/demo')
}, dispatch);

const SignupInfoContainer = connect(mapStateToProps, mapDispatchToProps)(SignupInfo);

export default SignupInfoContainer;

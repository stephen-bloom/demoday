import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Schedule from './Schedule';


const mapStateToProps = state => {
  return {

  }
};

const mapDispatchToProps = dispatch => bindActionCreators({

}, dispatch);

const ScheduleContainer = connect(mapStateToProps, mapDispatchToProps)(Schedule);

export default ScheduleContainer;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({

});

class Profile extends PureComponent {

  render() {
    return (
      <div>
        <h1>This is the user profile page</h1>
        <h2>If the user is a BA, they will be able to view and edit their profile</h2>
        <h2>If the user is a dispensary, they will be able to schedule a demo day here</h2>
      </div>
    );
  }
}

Profile.propTypes = {};

export default withStyles(styles)(Profile);

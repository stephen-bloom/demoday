import { updateObject } from '../utils';
import { userConstants } from '../constants';
import _ from 'lodash';


const initialState = {
  fetching: false,
  fetch_needed: true,
  current_user: undefined
};

/////////////////////
// Central Reducer //
/////////////////////

export const userReducer = (currentState = initialState, action) => {
  let update = {};

  if (_.includes(UserAsyncRequestTypes, action.type)) {
    update = UserAsyncRequest(currentState, action);
  }
  else if (_.includes(UserInsertTypes, action.type)) {
    update = UserInsert(currentState, action);
  }
  else if (_.includes(UserUIStateTypes, action.type)) {
    update = UserUIState(currentState, action);
  }
  else if (_.includes(UserErrorTypes, action.types)) {
    update = UserError(currentState, action);
  }

  return updateObject(currentState, update);
};


////////////////////
// Async Requests //
////////////////////

const UserAsyncRequestTypes = [
  userConstants.GET_PROFILE_REQUEST
];
const UserAsyncRequest = (currentState, action) => {
  let update = {};
  switch (action.type) {

    case userConstants.GET_PROFILE_REQUEST:
      update = {
        ...currentState,
        fetching: true
      };
      break;

    default: break;
  }
  return update;
};


////////////
// Insert //
////////////

const UserInsertTypes = [
  userConstants.GET_PROFILE_SUCCESS,
  userConstants.EDIT_PROFILE_SUCCESS
];
const UserInsert = (currentState, action) => {
  let update = {};
  switch (action.type) {

    case userConstants.EDIT_PROFILE_SUCCESS:
    case userConstants.GET_PROFILE_SUCCESS:
      update = {
        fetching: false,
        fetch_needed: false,
        current_user: action.profile
      };
      break;

    default: break;
  }
  return update;
};


//////////////
// UI State //
//////////////

const UserUIStateTypes = [];
const UserUIState = (currentState, action) => {
  let update = {};
  switch (action.type) {

    default: break;
  }
  return update;
};


///////////
// Error //
///////////

const UserErrorTypes = [
  userConstants.GET_PROFILE_FAILURE,
];
const UserError = (currentState, action) => {
  let update = {};
  switch (action.type) {

    case userConstants.GET_PROFILE_FAILURE:
      update = {
        fetching: false,
        fetch_needed: true,
        current_user: undefined
      };

      break;

    default: break;
  }
  return update;
};
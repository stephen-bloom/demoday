import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';

import HistoryPage from './HistoryPage';


const mapStateToProps = state => {
  return {

  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  demoNav: (id) => push('/demo/' + id)
}, dispatch);

const HistoryContainer = connect(mapStateToProps, mapDispatchToProps)(HistoryPage);

export default HistoryContainer;

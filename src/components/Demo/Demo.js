import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({

});

class Demo extends PureComponent {

  render() {
    const {
      id
    } = this.props;

    return (
      <div>
        <h1>This is the demo details page</h1>
        <h2>The id if this demo from the url is {id}</h2>
      </div>
    );
  }
}

Demo.propTypes = {
  id: PropTypes.string
};

export default withStyles(styles)(Demo);

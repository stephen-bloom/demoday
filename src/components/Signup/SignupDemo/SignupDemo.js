import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({

});

class SignupDemo extends PureComponent {

  render() {
    const {
      signupScheduleNav
    } = this.props;

    return (
      <div>
        <h1>This is where the user sets up their demo info</h1>
        <span onClick={signupScheduleNav}> Go to schedule setup</span>
      </div>
    );
  }
}

SignupDemo.propTypes = {
  signupScheduleNav: PropTypes.func
};

export default withStyles(styles)(SignupDemo);

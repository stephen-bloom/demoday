import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';

import Dashboard from './Dashboard';


const mapStateToProps = state => {
  return {

  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  settingsNav: () => push('/settings'),
  scheduleNav: () => push('/schedule'),
  historyNav: () => push('/history'),
  directoryNav: () => push('/directory'),
  profileNav: () => push('/user/placeholder')
}, dispatch);

const DashboardContainer = connect(mapStateToProps, mapDispatchToProps)(Dashboard);

export default DashboardContainer;

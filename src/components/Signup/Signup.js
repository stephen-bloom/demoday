import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import { Switch } from 'react-router-dom';

import ProtectedRoute from '../GlobalComponents/ProtectedRoute';
import SignupInfoContainer from './SignupInfo/SignupInfoContainer';
import SignupDemoContainer from './SignupDemo/SignupDemoContainer';
import SignupScheduleContainer from './SignupSchedule/SignupScheduleContainer';


const styles = (theme) => ({

});

class Signup extends PureComponent {

  render() {
    return (
      <Switch>
        <ProtectedRoute path='/signup/demo' component={SignupDemoContainer} />
        <ProtectedRoute path='/signup/schedule' component={SignupScheduleContainer} />
        <ProtectedRoute path='/signup' component={SignupInfoContainer} />
      </Switch>
    );
  }
}

Signup.propTypes = {};

export default withStyles(styles)(Signup);

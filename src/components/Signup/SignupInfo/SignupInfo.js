import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';


const styles = (theme) => ({

});

class SignupInfo extends PureComponent {

  render() {
    const {
      signupDemoNav
    } = this.props;

    return (
      <div>
        <h1>This is where the user enters in basic sign up info</h1>
        <span onClick={signupDemoNav}>Go to demo info setup</span>
      </div>
    );
  }
}

SignupInfo.propTypes = {
  signupDemoNav: PropTypes.func
};

export default withStyles(styles)(SignupInfo);

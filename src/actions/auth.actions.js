import { authConstants } from '../constants';


////
// UI
////


////
// Requests
////

export const loginRequest = (email, password) => ({
  type: authConstants.LOGIN_REQUEST,
  email,
  password
});

export const changePasswordRequest = (email, password) => ({
  type: authConstants.CHANGE_PASSWORD_REQUEST,
  email,
  password
});


////
// Insert
////

export const loginSuccess = (token, expires_at) => ({
  type: authConstants.LOGIN_SUCCESS,
  token,
  expires_at
});

export const changePasswordSuccess = (token, expires_at) => ({
  type: authConstants.CHANGE_PASSWORD_SUCCESS,
  token,
  expires_at
})


////
// Failure & Error
////

export const loginFailure = (error) => ({
  type: authConstants.LOGIN_FAILURE,
  error
});

export const changePasswordFailure = (error) => ({
  type: authConstants.CHANGE_PASSWORD_FAILURE,
  error
})
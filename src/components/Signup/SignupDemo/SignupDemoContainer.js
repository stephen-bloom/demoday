import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';

import SignupDemo from './SignupDemo';


const mapStateToProps = state => {
  return {

  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  signupScheduleNav: () => push('/signup/schedule')
}, dispatch);

const SignupDemoContainer = connect(mapStateToProps, mapDispatchToProps)(SignupDemo);

export default SignupDemoContainer;

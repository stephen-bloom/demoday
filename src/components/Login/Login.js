import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';

import LoginFormContainer from './LoginForm/LoginFormContainer';


const styles = (theme) => ({

});

class Login extends PureComponent {

  submit = values => {
    const email = values['email'];
    const password = values['password'];

    if (email && password) {
      this.props.login(email, password);
    }
  };

  render() {
    const {
      loginClick
    } = this.props;

    return (
      <div>
        <LoginFormContainer />
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func
};

export default withStyles(styles)(Login);

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Demo from './Demo';


const mapStateToProps = (state, ownProps) => {

  return {
    id: ownProps.match.params.demoId
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({

}, dispatch);

const DemoContainer = connect(mapStateToProps, mapDispatchToProps)(Demo);

export default DemoContainer;
